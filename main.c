#include <ev3.h>
#include <stdlib.h>
#include "snake.h"

#define WIDTH 50
#define HEIGHT 15
#define SLEEPTIME 300

/*

!!! UNTESTED !!!
WIDTH AND HEIGHT HAVE TO BE ADJUSTED TO FIT THE SCREEN

*/

struct termbuffer disp;
struct snake sn;

// draw termbuffer to display
void draw_display() {
	LcdClean();
	LcdPrintf("\f");
	int i, k;
	for(i = 0; i < disp.height; i++) {
		for(k = 0; k < disp.width; k++) {
			char c = disp.buffer[i*disp.width+k];
			if(c != 0) LcdPrintf("%c", c);
			else LcdPrintf(" ");
		}
		LcdPrintf("\n");
	}
}

// handle user input and calculate snake logic
void tick() {

	if(ButtonIsDown(BTNLEFT)) {
		set_mov_dir(&sn, SN_DIR_LEFT);
	}
	else if(ButtonIsDown(BTNRIGHT)) {
		set_mov_dir(&sn, SN_DIR_RIGHT);
	}
	else if(ButtonIsDown(BTNUP)) {
		set_mov_dir(&sn, SN_DIR_UP);
	}
	else if(ButtonIsDown(BTNDOWN)) {
		set_mov_dir(&sn, SN_DIR_DOWN);
	}

	snake_act(&sn, disp);
}


int main() {

	srand((unsigned)time(NULL));

	disp = make_termbuffer(WIDTH, HEIGHT);
	clear_termbuffer(&disp);

	// init snake in the middle of the screen
	sn = make_snake(disp.width/2, disp.height/2);
	// set food to random position
	set_foodpos(rand()%disp.width, rand()%disp.height);

	InitEv3();

	// run until exit button is pressed or snake dies
	while(ButtonIsUp(BTNEXIT)) {
		if(sn.dead) break;
		// reset food when consumed last frame
		if(sn.food.x == -1 && sn.food.y == -1) set_foodpos(rand()%disp.width, rand()%disp.height);

		tick();
		draw_display();

		Wait(SLEEPTIME);
	}

	LcdClean();
	LcdPrintf("\fGame Over");

	Wait(3000);

	FreeEv3();
	return 0;
}