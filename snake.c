#include "snake.h"

// initialize new snake object with x|y pos and default values
struct snake make_snake(int x, int y) {
	struct snake sn;
	sn.dead = 0;
	sn.length = 1;
	sn.body[0].x = x;
	sn.body[0].y = y;
	sn.mov.x = 0;
	sn.mov.y = 1;
	sn.head = point_add(sn.mov, sn.body[0]);
	return sn;
}

// set food position
void set_foodpos(struct snake *sn, int x, int y) {
	sn->food.x = x;
	sn->food.y = y;
}

// calculate movement and "collision"
void snake_act(struct snake *sn, struct termbuffer disp) {
	// check food collision
	if(point_eq(sn->head, sn->food)) {
		sn->length++;
		// set food to -1|-1 for reset in higher function
		set_foodpos(sn, -1, -1);
	}

	// move body forwards
	int i;
	for(i = sn->length-1; i > 0; i--) {
		point_cp(&(sn->body[i-1]), &(sn->body[i]));
	}
	point_cp(&(sn->head), &(sn->body[0]));

	sn->head = point_add(sn->body[0], sn->mov);

	// check self collision
	for(i = 1; i < sn->length; i++) {
		if(point_eq(sn->body[0], sn->body[i])) {
			sn->dead = 1;
		}
	}

	// check wall collision
	if(sn->body[0].x < 0 || sn->body[0].y < 0 || sn->body[0].x >= disp.width || sn->body[0].y >= disp.height) {
		sn-> dead = 1;
	}

}

// render snake body and food to termbuffer
void render_snake(struct termbuffer *disp, struct snake sn) {
	// draw body to buffer
	int i;
	for(i = 0; i < sn.length; i++) {
		set_char(disp, sn.body[i], 'B');
	}

	// draw head to buffer
	set_char(disp, sn.head, 'H');

	// draw food to buffer
	set_char(disp, sn.food, 'O');
}

// set movement direction
void set_mov_dir(struct snake *sn, int dir) {
	switch(dir) {
	case SN_DIR_LEFT:
		sn->mov.x = -1;
		sn->mov.y = 0;
		break;
	case SN_DIR_RIGHT:
		sn->mov.x = 1;
		sn->mov.y = 0;
		break;
	case SN_DIR_UP:
		sn->mov.y = 0;
		sn->mov.y = -1;
		break;
	case SN_DIR_DOWN:
		sn->mov.x = 0;
		sn->mov.y = 1;
		break;
	}
}