#include "termbuffer.h"

#define SN_DIR_LEFT 0
#define SN_DIR_RIGHT 1
#define SN_DIR_UP 2
#define SN_DIR_DOWN 3

struct snake {
	// 0 = alive, >0 = dead
	int dead;
	int length;
	// movement is added to current head position
	struct point mov;
	// position for the next frame
	struct point head;
	struct point body[32];
	struct point food;
};

struct snake make_snake(int x, int y);
void snake_act(struct snake *sn, struct termbuffer disp);
void render_snake(struct termbuffer *disp, struct snake sn);
void set_foodpos(struct snake *sn, int x, int y);
void set_mov_dir(struct snake *sn, int dir);