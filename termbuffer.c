#include "termbuffer.h"
#include <stdlib.h>

// makes x and y positive
void point_abs(struct point *p) {
	if(p->x < 0) p->x *= -1;
	if(p->y < 0) p->y *= -1;
}

// add p1 and p2 and return the result
struct point point_add(struct point p1, struct point p2) {
	struct point p_new;
	p_new.x = p1.x + p2.x;
	p_new.y = p1.y + p2.y;
	return p_new;
}

// subtract p1 and p2 and return the result
struct point point_sub(struct point p1, struct point p2) {
	struct point p_new;
	p_new.x = p1.x - p2.x;
	p_new.y = p1.y - p2.y;
	return p_new;
}

// calculate vector from p_from to p_to
struct point point_vec(struct point p_from, struct point p_to) {
	return point_sub(p_to, p_from);
}

// copy x and y from p_from to p_to as reference
void point_cp(struct point *p_from, struct point *p_to) {
	p_to->x = p_from->x;
	p_to->y = p_from->y;
}

// return 1 if p1 and p2 are at the same position else return 0
int point_eq(struct point p1, struct point p2) {
	if(p1.x == p2.x && p1.y == p2.y) return 1;
	return 0;
}

// initialize termbuffer for width and height
struct termbuffer make_termbuffer(int width, int height) {
	struct termbuffer disp;
	disp.width = width;
	disp.height = height;
	disp.i_max = width*height;
	disp.buffer = (char*)malloc(width*height*sizeof(char));
	return disp;
}

// set all values in buffer to (byte) 0
void clear_termbuffer(struct termbuffer *disp) {
	int i = 0;
	for(i = 0; i < disp->i_max; i++) {
		disp->buffer[i] = 0;
	}
}

// set all values in buffer to (char) c
void fill_termbuffer(struct termbuffer *disp, char c) {
	int i = 0;
	for(i = 0; i < disp->i_max; i++) {
		disp->buffer[i] = c;
	}
}

// set buffer at pos to char
void set_char(struct termbuffer *disp, struct point pos, char c) {
	int index = pos.y*disp->width + pos.x;

	if(pos.x < 0 || pos.y < 0 || pos.x >= disp->width || pos.y >= disp->height) return;

	disp->buffer[index] = c;
}