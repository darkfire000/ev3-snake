struct point {
	int x;
	int y;
};

void point_abs(struct point *p);
struct point point_add(struct point p1, struct point p2);
struct point point_sub(struct point p1, struct point p2);
struct point point_vec(struct point p_from, struct point p_to);
void point_cp(struct point *p_from, struct point *p_to);
int point_eq(struct point p1, struct point p2);

struct termbuffer {
	int width;
	int height;
	// i_max = width*height
	int i_max;
	// allocate memory for i_max bytes for buffer via make_termbuffer() method
	char *buffer;
};

struct termbuffer make_termbuffer(int width, int height);
void clear_termbuffer(struct termbuffer *disp);
void fill_termbuffer(struct termbuffer *disp, char c);
void set_char(struct termbuffer *disp, struct point pos, char c);