#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "snake.h"

struct termbuffer disp;
struct snake sn;

int abc = 0;

void render_disp() {
	int i, k;
	for(i = 0; i < disp.height; i++) {
		for(k = 0; k < disp.width; k++) {
			char c = disp.buffer[i*disp.width+k];
			if(c != 0) printf("%c", c);
			else {
				if(i == 0 || i == disp.height-1 || k == 0 || k == disp.width-1) printf("#");
				else printf(" ");
			}
		}
		printf("\n");
	}
}

void tick() {
	if(abc == 2) set_mov_dir(&sn, SN_DIR_LEFT);
	snake_act(&sn, disp);

	abc++;
}

int main() {
	
	disp = make_termbuffer(70, 15);
	clear_termbuffer(&disp);
	sn = make_snake(20, 4);
	sn.length = 1;
	set_foodpos(&sn, 20, 7);

	while(1) {
		clear_termbuffer(&disp);
		tick();
		render_snake(&disp, sn);
		render_disp();

		if(sn.dead) {
			break;
		}

		Sleep(1000);
	}

	printf("%s\n", "game over");

	return 0;
}